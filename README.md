# Overview #
This repository contains a set of code, samples, libraries, scripts and documentation for the Wisen Talk² Whisper Node - AVR, an ultra-low power version of the Arduino with built-in Wireless communication and capable of running for years on a single AA battery.

For additional information about this product or about the Talk² project, please visit:

* Wisen Website and On-line Store at http://wisen.com.au
* Our official discussion Forum: http://talk2forum.wisen.com.au

![board_antenna_mini.jpg](https://bitbucket.org/repo/drxzG7/images/1440280951-board_antenna_mini.jpg)

|                 |                                        |
|----------------:|:---------------------------------------|
|**Product:**     |Talk² Whisper Node - AVR                |
|**Version:**     |1.0v - Official Release                 |
|**Release Date:**|10 September 2017                       |
|**Status:**      |Shipping                                |

*Use GIT Tags to navigate between different versions/releases.*

## Special Notes ##
Here some important notes regarding the Whisper Node:

* **This is a 3.3V board** - Although the MCU (Atmega328p) can run at 5V, there are components like the external SPI Flash and the RFM69 Radio Module that will not tolerate 5V. If you need to connect a 5V device to any of the pins, make sure the same pin is not shared with the external Flash neither the RFM69.
* **FTDI Voltage** - When powering the board via the FTDI pins, make sure the supplied voltage is between 3.4V and 6V as this will be using the LDO regulator (same as VIN).
* **Quick start** - If you are looking how to setup your Arduino IDE to work with this board, jump straight into the "Software" topic on this page to know how to install the Whisper Node Board, Talk² Library and other required libraries to run the examples.

## Table of Contents ##

[TOC]

# Hardware #

## Specifications ##

### Dimensions and Connections ###
* Board size: 65.65 x 26.75 x 1.6mm + connectors overhanging
* GPIOs: Two 17 Pin PCB Header to access all MCU Pins, Vin, Vbat, and both 3.3V Rails
* Standard 3x2 ISP Header
* 6 Pin FTDI Header
* Micro USB for External Power supply
* Pico-blade 1.25mm for Battery Connection
* RF SMA Connection for external Antenna
 
### Components ###
* MCU Atmel 8-bit AVR: ATMega328P-AU (32Kb Flash/2Kb RAM)
* 4Mbit SPI Flash: W25X40
* Sub-GHz RF Module: RFM69
* Step-up Switching Regulator: MCP16251
* Dual LDO Regulator: AP7332
* 16Mhz high precision Crystal
* 2x Feedback LEDs (Blue and Yellow)
* 2x Tactile User Buttons
* 2dB omni-directional SMA antenna
 
### Optional Upgrade Kits ###
* CR2032 Battery Holder
* Real-Time Clock Kit: Maxim DS3231M
* High Input Voltage LDO 1117 Kit

## Board Layout ##
The layout below shows the position for the most important connectors and components.

```
#!
             ┌─────  26.65mm  ─────┐

                   ╓───╖
                  ┌╢   ╟┐     26.65x64.65mm
             ╔════╡ CN3 ╞══════════╗        ┐
             ║┌─┐ └─────┘┌┐┌┐(H)┌─┐║        │ <--- Hole 2
             ║│■│     LD1└┘└┘LD2│■│║        │   20.0x61.5mm
             ║│o│               │o│║        │
             ║│o│               │o│║        │
             ║│o│               │o│║        │
             ║│o│<--P1     P2-->│o│║        │
             ║│o│               │o│║        │
             ║│o│               │o│║        │
             ║│o│  ┌───┐ ┌───┐  │o│║        │
             ║│o│  │BT1│ │BT2│  │o│║        │
             ║│o│  └───┘ └───┘  │o│║
             ║│o│               │o│║      64.65mm
             ║│o│               │o│║
             ║│o├───┐<--P3:ISP  │■│║        │
             ║│o│o o│         ┌─┤o│║        │
             ║│■│o o│P6:RTC-->│o│o│║        │
             ║│o├─┬─┴─────────┤o│o│║        │
             ║│o│ │o o o o o ■│o│o│║        │
             ║└─┘ └───────────┴─┴─┘║        │         Mounting Hole Diameter: 2.75mm
            ┌╨──╖     P4:FTDI      ║        │         Recommended Screw: Nylon M2.5
            │CN1║                  ║        │
            └╥──╜                  ║        │
Hole 1 -->  ┌╨╖  (H)           ┌───╢        │
7.8x10.3mm  └╥╜CN2    P5:VCC-->│o ■║        │
             ╚═════════════════╧═══╝        ┘
           0x0mm
```

### Pinout ###
The Talk² Whisper Node has many headers and connectors. All pins are positioned one a 2.54 grid and all PCB Headers are shipped with the board, making it compatible with a breadboard.

Find below the pin-out and details about each one.

#### Extension Headers - P1 and P2 ####
The extension headers provide direct access to the MCU pins. Pins already in use by the Whisper Node board will have their function displayed as [BOARD_USED]. Board functions displayed as (BOARD_OPTIONAL) are optional and not connected by default.

Note that some of the board functions can be disabled by cutting PCB trace or removing component, look for more information about that on this document.
```
#!
                                                  P1            P2
                                                   ┌─┐           ┌─┐
                                           GND ────│■│           │■│──── GND
            [RFM69_INT]         INT0:32:PD2:D2 ────│o│           │o│──── AREF:20
              (RTC_INT)      INT1/PWM:1:PD3:D3 ────│o│           │o│──── D1:PD1:31:TXD
                 [BTN1]               2:PD4:D4 ────│o│           │o│──── D0:PD0:30:RXD
                 [BTN2]           PWM:9:PD5:D5 ────│o│           │o│──── A7:ADC7:22                [VIN_Voltage]
                 [LED1]          PWM:10:PD6:D6 ────│o│           │o│──── A6:ADC6:19                [VBAT_Voltage]
            [RFM69_RST]              11:PD7:D7 ────│o│           │o│──── A5/D19:PC5:28:ADC5/SCL
            [W25X40_CS]              12:PB0:D8 ────│o│           │o│──── A4/D18:PC4:27:ADC4/SDA
                 [LED2]          PWM:13:PB1:D9 ────│o│           │o│──── A3/D17:PC3:26:ADC3
             [RFM69_CS]      SS/PWM:14:PB2:D10 ────│o│           │o│──── A2/D16:PC2:25:ADC2
         [RFM69/W25X40]    MOSI/PWM:15:PB3:D11 ────│o│           │o│──── A1/D15:PC1:24:ADC1        (RTC_VIN)
         [RFM69/W25X40]        MISO:16:PB4:D12 ────│o│           │o│──── A0/D14:PC0:23:ADC0        [VBAT_Voltage_Mosfet]
         [RFM69/W25X40]         SCK:17:PB5:D13 ────│o│           │■│──── GND
                                    29:PC6:RST ────│o│           │o│──── 3V3_R1
                                           GND ────│■│           │o│──── 3V3_R1
                                           VIN ────│o│           │o│──── 3V3_R2
                                          VBAT ────│o│           │o│──── 3V3_R2
                                                   └─┘           └─┘

```
#### Antenna - CN3 ####
The 50Ω SMA Female connector is attached to the RFM69 radio module. To prevent any damage to the RF module, make sure there's always an antenna connected when transmitting.

To improve the range a bigger or directional antenna might be used. Remember to keep the cable as short as possible to minimize signal lost.

Whisper Node is normally shipped with a 2dB whip antenna.

#### Micro USB - CN1 and Picoblade - CN2 ####
The Talk² Whisper Node can be powered by Battery (VBAT) or an external Power Supply (VIN). The Micro USB connector is the standard way to connect a PSU to the board and the supplied voltage must be between 3.7V and 6V, a mobile phone USB A/C Charger or a USB power bank works pretty well.

For the battery connection, a power source between 0.9V and 3.3V can be attached to the micro connector Molex Picoblade. A 2xAA battery holder is the perfect example.

More detaild about the powering options can be found further on this document.
```
#!
          ║
          ║
         ┌╨───────╖*
         │       ─╢──── 1:VIN
         │       ─╢──── 2:NC
         │  CN1  ─╢──── 3:NC
         │       ─╢──── 4:NC
         │       ─╢──── 5:GND
         └╥───────╜
          ║
         ┌╨────╖*
         │    ─╢──── 1:VBAT
         │ CN2 ║
         │    ─╢──── 2:GND
         └╥────╜
          ╚═══════════

```

#### Programming Interfaces ISP - P3 and FTDI - P4 ####
Those are the headers to access ISP and FTDI connections to the MCU. It's important to note that although the MCU is 5V compatible the Radio Module RFM69 and the Flash SPI memory chip are not. For that reason never connect a 5V ISP programmer to the board or it might damage those components.
```
#!
           P3 - ICSP                   P4 - FTDI
     ┌───────────────────┐       ┌─────────────────────┐

          ┌──── 5:RESET             ┌─────────────┐*
          │ ┌── 3:SCK               │ o o o o o ■ │
          │ │ ┌ 1:MISO              └─────────────┘
          │ │ │                       │ │ │ │ │ │
        ┌───────┐*                    │ │ │ │ │ └ 1:GND
        │ o o o │                     │ │ │ │ └── 2:NC
        │ ■ o o │                     │ │ │ └──── 3:VIN
        └───────┘                     │ │ └────── 4:RXD
          │ │ │                       │ └──────── 5:TXD
          │ │ └ 2:VIN                 └────────── 6:DTR
          │ └── 4:MOSI
          └──── 6:GND

```
### Buttons and LEDs ###
The Talk² Whisper Node Buttons are not simple tactile switches connected to the MCU Pins, they actually include a small debounce circuit. If you're not familiar with the concept, a debounce circuit prevents a switch to oscillate between states at high frequency when pressed, smoothing its operation.

![WhisperNode_BT_Debounce_Circuit.png](https://bitbucket.org/repo/drxzG7/images/1524179939-WhisperNode_BT_Debounce_Circuit.png)

| Designator | Normal | Active | MCU Pin | Low to High | High to Low |
|------------|--------|--------|---------|-------------|-------------|
| BT1        | LOW    | HIGH   | D4      | 0.5 ms      | 8.0 ms      |
| BT2        | LOW    | HIGH   | D5      | 0.5 ms      | 8.0 ms      |

*Note that both BT1 and BT2 are normal LOW, active HIGH. This has been designed like that to minimize any leakage but manly to be a bit more human friendly as an active LOW switch might be confusing.*

![WhisperNode_BT_Debounce_Low-High.png](https://bitbucket.org/repo/drxzG7/images/2067308204-WhisperNode_BT_Debounce_Low-High.png)
![WhisperNode_BT_Debounce_High-Low.png](https://bitbucket.org/repo/drxzG7/images/2877258136-WhisperNode_BT_Debounce_High-Low.png)

For reference, there's a interesting debounce online calculator: http://protological.com/debounce-calaculator/ which can assist you to calculate the resistor and capacitor values when designing such circuit.

For the LEDs, the board has two and they are simple 0603 LEDs with resistors at the Anode side, directly connected to a MCU pin:

| Designator | Color   | Resistor | MCU Pin |
|------------|---------|----------|---------|
| LD1        | Blue    | 220R     | D6      |
| LD2        | Yellow  | 220R     | D9      |

*As a good-practice suggestion to preserve power, never lit a LED and perform a radio transmission at the same time. This will help to keep the maximum board consumption current as low as possible. Also, a 10ms to 25ms blink is normally more than enough to be noticed by most humans and will keep the energy consumption low.*

### Add-Ons RTC, LDO and Coin-Battery ###
To make use of all PCB real estate, the Talk² Whisper Node has a few blank Pads. With the correct components the board functionalities can be easily extended to add the following features like:

* I²C Real Time Clock with built-in Crystal and Temperature Sensor
* Extra 1117 LDO Regulator for Input Voltages over 12V
* CR2032 Coin-Cell battery Holder (can't be used together with 1117 LDO)

#### LDO - P5 ####
Even with Talk² Whisper Node being designed to run on batteries, it also can be powered by an external PSU limited to 5.5V. In case the supplied voltage is grater than 5.5V, a standard 1117 LDO Regulator can be soldered to the back of the board together with input and output 0805 ceramic capacitors.

The 1117 LDOs are not the most efficient regulators, with higher quiescent current and significant drop-out. The good thing about those devices is that the maximum input voltage is normally between 15V and 21V, depending on the manufacturer/model, being a great option when powering from a 12V source for example.

The 1117 LDO output is connected directly to the VIN, before the input Diode, the same way an external USB power supply would be connected. For that reason independent of the model used it must be output 5.5V and supply a minimum of 700mA.

**Attention:*** do not power the board through an 1117 LDO and an external power supply connected to the Micro USB at same time! But it's OK to power the board via the 1117 LDO and have a battery connected to VBAT working, as a power backup.

Suggested parts:

* Diodes AZ1117IH-5.0TRG1 - http://www.diodes.com/catalog/Single_LDOs_50/AZ1117_10276

##### Assemble #####

* LDO 1117 - SOT223: **U5**
* Ceramic Capacitor 10uF 35V - SMD 0805: **C19 and C20**
* PCB Block - 2.54mm: **P5**

#### RTC - P6 ####
The Talk² Whisper Node has a SOIC-8 footprints on the back to add an I²C Real Time Clock with built-in Crystal. The footprint matches the Maxim DS3231MZ+ (https://datasheets.maximintegrated.com/en/ds/DS3231M.pdf) and the Maxim DS3232MZ+ (https://datasheets.maximintegrated.com/en/ds/DS3232M.pdf) is also compatible.

If necessary special ports from the RTC chip can be accessed via P6 described below:

```
#!
       ┌─┐
       │o│──── RST
       │o│──── INT/SQW
       │o│──── 32kHz
       └─┘
```

The RTC chip has two power inputs, the VCC and VBAT. Normally when running by VCC the RTC consumes more power but it also has all features turned on. On the other hand, when powered exclusively by VBAT the chip will run in time-keeping mode only to save energy but still responding to I²C communication.

By default the RTC VCC pin is connected to the 3V3_R2 and the VBAT to the 3V3_R1 on the Talk² Whisper Node. When the board is powered only by battery, the VCC will be off, forcing the Clock to run in low-power mode.

The board also makes possible to connect the VCC to the MCU pin A1 using a solder jumper, so the RTC VCC can be powered by turning the A1 Pin HIGH/LOW via software. Additionally the RTC INT can be connected to the MCU D3/INT1 Pin via a solder jumper as well, so the RTC Alarms can be used to trigger interrupts on the MCU.

##### Assemble #####

* Maxim DS3231M - SOIC-8: **U6**
* Ceramic Capacitor 0.1uF 6V - SMD 0603: **C21 and C22**
* Film Resistor 100K - SMD0603: **R28 and R29**
* Film Resistor 4.7K - SMD0603: **R30 and R31**
* PCB Headers - 2.54mm: **P6** *(optional)*

#### Coin-Cell - BAT1 ####
For ultra-low power applications, where size and weight matters, a small CR2032 coin-cell might be the best option. In this case a SMD battery retainer can be soldered to the board. The compatible parts are:

* BK-912 - http://www.batteryholders.com/part.php?pn=BK-912
* BAT-HLD-001 - https://www.linxtechnologies.com/en/products/connectors/battery-holders

The coin-cell battery terminals are attached directly to the step-up switching regulator, the same way the Picoblade - CN2 is. The coin-cell retainer shares the same PCB space as the 1117 LDO, so having both installed on the same board is not possible.

Before soldering the battery holder, make sure both round pads identified as "BAT1" are covered with a thin layer of solder. This is necessary to raise the contact pads, as well to prevent the battery terminal touching any other footprints.

Bear in mind that a CR2032 can supply 3V for most of it's life but the continuous current delivery is very limited. Simple coding strategies to minimize high peak current draw, like not blinking the LED at the same time the RFM69 module is being used, will significantly extend the battery's life.

### Modifying Board Configuration ###
To give extra flexibility some of the MCU Pins on the Whisper Node are connected/disconnected through Solder Jumpers. In a similar way some components, like buttons and LEDs, can be easily and safety disconnected to free GPIOs according to the user's needs.

#### Via Solder Jumpers ####
Solder jumpers are specially designed cooper pads which can be easily cut or bridged using solder to change the physical configuration of the board. All solder jumpers are located at the back on the board. The table below describes all available jumpers:

|Jumper|Default State|Purpose                   |Note                                                                                  |
|------|-------------|--------------------------|--------------------------------------------------------------------------------------|
|JP1   |Closed       |Battery Voltage           |Connects the battery resistor divider to the MCU A6 Pin                               |
|JP2   |Closed       |Vin Voltage               |Connects the Vin resistor divider to the MCU A7 Pin                                   |
|JP3   |Closed       |Battery Voltage Mosfet    |Controls the Mosfet (via A0) that allows the battery voltage resistor to read voltage*|
|JP4   |Closed       |Powers the RTC from 3V3R2 |Caution when using JP4 and JP5 at same time                                           |
|JP5   |Open         |Powers the RTC from MCU A1|Can be used to power the RTC Vin from one of the MCU pin to enable all RTC features   |
|JP6   |Open         |RTC Int to MCU D3 (Int1)  |This can be used to connect the RTC Interrupt pin to the MCU Int1 to use alarms       |
|JP7   |Closed       |RFM69 Reset PIN to MCU D7 |Used to hardware reset the RFM69 module and bring it to a known state                 |

**This exists to minimize current leakage thought the voltage divider when it's not being read.*

#### Via Resistors ####
Because of the density of the board, not all connection can be routed through Solder Jumpers. But additional modifications can be done to re-purpose MCU Pins without causing any damage to the board. The table below shows individual resistors safe to be remove to disable components like an LED or Button.

|Resistor/Capacitor|Value      |Purpose             |Note                                                        |
|------------------|-----------|--------------------|------------------------------------------------------------|
|R1                |220R       |LED 1 - Blue        |Safe to remove resistor to disable LED and free the MCU Pin |
|R2                |220R       |LED 2 - Yellow      |Safe to remove resistor to disable LED and free the MCU Pin |
|R6 + C8           |10K + 0.1uF|BT 1                |Disables Button. Removal of the Capacitor is optional       |
|R8 + C9           |10K + 0.1uF|BT 2                |Disables Button. Removal of the Capacitor is optional       |

**Note that most of the components are size 0402, so a good pair of fine tweezers, a reliable solder iron and special care must be used when performing any modification to the board.*

### Schematics ###
Most of the Talk² Whisper Node schematic is available at the "Documentation" folder on this repository - https://bitbucket.org/talk2/whisper-node-avr/src/master/Documentation.

## Power ##
The Talk² Whisper Node can be powered by different sources or even combine multiple ones. The board has been specially designed to run on batteries, more specific from Alkaline cells. For that reason the board counts with two voltage regulators, a Microchip MCP16251 step-up switching regulator and a Diodes Dual LDO AP7332.

The board offers two 3.3V rails:

* 3V3R1 is the main rail and it's used to power all board components (MCU, Radio, Flash, etc)
* 3V3R2 is the auxiliary rail and is only active when the board is powered from the LDO (VIN)

### Battery: VBAT ###
The Battery input is directly connected to the MCP16251 step-up regulator. To be as efficient as possible there's no diode or protection on this line, do not reverse the polarity. The VBAT can be powered via the micro connector Moled Picoblade or from any PCB VBAT Pad - check the board pinout for details.

Although the board has been tested and able to run down to 0.75V, the recommended/start voltage must be between 0.9V and 3.3V.

*Note: 3R3V2 is not available when the board is powered by VBAT only.

### Power Supply: VIN ###
The VIN is connected to the LDO AP7332 thorough a schottky diode. The supplied voltage must be between 3.7V and 6V. The VIN can be powered via the Micro USB connector or from any PCB VIN Pad - check the board pinout for details.

### By-Passing the Regulators ###
An alternative way to power the board is supplying 3.3V directly to one of the 3V3R1 pins. Note by doing that you'll be by-passing any kind of built-in regulation, so make sure the power source is stable or it might cause permanent damage to the board.

### Power Backup ###
It's important to highlight that the VIN will take precedence over VBAT. In other words, once a power supply is connected to VIN, it'll disable the step-up regulator by pulling its enable pin low. In this configuration the battery consumption will be as low as 3uA.

In case the power supply is disconnected, or it stop working, the step-up regulator will be re-enabled and will start feeding 3.3V to the 3V3R1.

### Voltage Monitor ###
The Talk² Whisper Node counts with two resistor dividers configure as R Top: 562K and R Bot: 100K. This will result at 0.151V output for every 1V input. One resistor divider is attached to the VIN with the output to A7 and the other is attached to the VBAT through a P+N Mosfet, with the output to A6.

Both voltage dividers can bee used to monitor the input voltage levels via the MCU ADC and used to make the application behave accordingly to the available power sources. For the VBAT voltage divider, it's not always connect for power-saving. To read the VBAT voltage divider it's necessary first to bring the VBAT_Voltage_Mosfet pin high - check the board pinout for details.

Using the Internal MCU analog reference of 1.1V is possible to read up to 7.282V on any of the voltage dividers. The MCU counts with a 10bit resolution ADC, in other words, it can have a value from 0 to 1023. In a practical example, if the ADC is reading 211, it means the voltage before the voltage divider is around 1.5V:

```
Voltage = (MAX_Voltage * ADC_Reading) / 1024
Voltage = (7.282 * 211) / 1024
Voltage = 1.5
```
### Board Power Consumption ###
The board consumption was measured at the Battery Input (Step-up Regulator), but instead of a battery, an adjustable bench power supply was used to simulate multiple voltages.

**Test Modes**

* **All Sleeping:** MCU in Power-down sleeping mode, BOD and ADC disabled. SPI Flash and RFM69 Radio Module in Sleep modes;
* **All Sleeping + RTC:** Same as above plus optional RTC Chip soldered on the board and running in time-keeping mode;
* **MCU Running:** MCU in a "delay()" looping with all peripherals turned on. SPI Flash and RFM69 Radio Module in Sleep modes.

|Input Voltage|Mode                                  |Current|
|-------------|--------------------------------------|-------|
|3V           |All Sleeping                          |3.7µA  |
|3V           |All Sleeping + RTC                    |9.6µA  |
|3V           |MCU Running, Flash and Radio Sleeping |7.8mA  |
|2.5V         |All Sleeping                          |4.3µA  |
|2.5V         |All Sleeping + RTC                    |11.8µA |
|2.5V         |MCU Running, Flash and Radio Sleeping |9.8mA  |
|1.5V         |All Sleeping                          |5.4µA  |
|1.5V         |All Sleeping + RTC                    |15.5µA |
|1.5V         |MCU Running, Flash and Radio Sleeping |17.6mA |
|1V           |All Sleeping                          |6.3µA  |
|1V           |All Sleeping + RTC                    |18.2µA |
|1V           |MCU Running, Flash and Radio Sleeping |27.8mA |
|0.8V         |All Sleeping                          |8.1µA  |
|0.8V         |All Sleeping + RTC                    |25.6µA |
|0.8V         |MCU Running, Flash and Radio Sleeping |36.2mA |

*Note that the figures might have some variations (+- 2µA), which can be caused by factors like temperature, power supply stability, etc. Make sure the voltage is measured as close as possible to the board.*

### RFM69 Power Consumption ###
The Radio module is the component which consumes more energy, specially during transmission. To reduce power usage, a few methods can be implemented:

* Reduce TX power to minimum required;
* Reduce the message size, so less time is spent sending the message;
* Use highest TX speed possible;
* Use "sleep mode" as much as possible.

Below you can find a few oscilloscope captures showing the power consumption using different TX Output power. The sent message was 20 bytes long, being 8 bytes of payload and 12 bytes for headers and CRC added by the module. Only the first capture contains all labels and details but the parameters for all captures are the same.

![RFM69_13dBm_10dBm_7dBm.png](https://bitbucket.org/repo/drxzG7/images/2609502644-RFM69_13dBm_10dBm_7dBm.png)
![RFM69_13dBm_4dBm_0dBm.png](https://bitbucket.org/repo/drxzG7/images/43709075-RFM69_13dBm_4dBm_0dBm.png)
![RFM69_13dBm_-3dBm_-7dBm.png](https://bitbucket.org/repo/drxzG7/images/1618168630-RFM69_13dBm_-3dBm_-7dBm.png)
![RFM69_13dBm_-13dBm_-18dBm.png](https://bitbucket.org/repo/drxzG7/images/3919269816-RFM69_13dBm_-13dBm_-18dBm.png)

*All measurements were taken at the 3V3_R1, just after the regulator output.
The scale is 100mV = 10mA, for example: the 13dBm peak is located at the top of the 6th division, which is read as 600mV and is equal to 60mA.*

### Regulator Efficiency ###
Below some test results from the Power Supply unit only. The tests were performed on a Talk² Whisper Node assembled only with the Power Supply components. The current was measured at the Input with a no-load and 100Ohms resistive load configuration.

#### Step-up ####
|Input Voltage|Output Voltage|Load*   |Current|Efficiency|
|-------------|--------------|--------|-------|----------|
|3V           |n/a           |shutdown|3.9µA  |n/a       |
|3V           |3.27V         |no-load |3.2µA  |n/a       |
|3V           |3.26V         |100Ω    |37.1mA |97.8%     |
|2.5V         |n/a           |shutdown|3.3µA  |n/a       |
|2.5V         |3.27V         |no-load |3.6µA  |n/a       |
|2.5V         |3.26V         |100Ω    |45.9mA |94.9%     |
|1.5V         |n/a           |shutdown|1.9µA  |n/a       |
|1.5V         |3.27V         |no-load |4.6µA  |n/a       |
|1.5V         |3.25V         |100Ω    |80mA   |90.7%     |
|1V           |n/a           |shutdown|1.4µA  |n/a       |
|1V           |3.27V         |no-load |5.7µA  |n/a       |
|1V           |3.25V         |100Ω    |140mA  |77.7%     |
|0.8V         |n/a           |shutdown|1.2µA  |n/a       |
|0.8V         |3.27V         |no-load |5.7µA  |n/a       |
|0.8V         |3.25V         |100Ω    |180mA  |75.6%     |

**The shutdown load represents the current consumption when the board is powered by the LDO regulator, which will place the Step-up in shutdown mode.*

#### LDO and 1117 ####
|Input Voltage|Output Voltage|Load    |Current|Regulator    |
|-------------|--------------|--------|-------|-------------|
|5V           |3.28V         |no-load |62.0µA |Built-in LDO |
|5V           |3.28V         |100Ω    |33.5mA |Built-in LDO |
|7.2V         |3.28V         |no-load |5.8mA  |Optional 1117|
|7.2V         |3.28V         |100Ω    |38.1mA |Optional 1117|
|9V           |3.28V         |no-load |5.8mA  |Optional 1117|
|9V           |3.28V         |100Ω    |38.0mA |Optional 1117|
|12V          |3.28V         |no-load |5.8mA  |Optional 1117|
|12V          |3.28V         |100Ω    |36.8mA |Optional 1117|

All tests where performed at room temperature, around 25°C. The power source used was a Tenma 72-10480 and to measurement of the Step-up regulator was done through a µCurrent (https://www.eevblog.com/projects/ucurrent/). Also, all connections were kept as short as possible to reduce any voltage-drop across the wires.

# Software #
This section describes a bit about the standard/factory firmware as well the MCU Fuses and Bootloader configuration.

## Arduino IDE Boards ##
If you're using the Arduino IDE, you'll need a "Board Configuration" to upload code to the Whisper Node. This can be done by adding the following "Additional Boards Manager URL" to your Arduino IDE preferences: http://talk2arduino.wisen.com.au/master/package_talk2.wisen.com_index.json

For additional information, please refer to the https://bitbucket.org/talk2/arduino-ide-boards repository.

## Library ##
The Talk² Arduino Library can be installed using the "Library Manager" on the Arduino IDE. For all details and access to the library source code, please refer to the https://bitbucket.org/talk2/talk2-library repository.

## MCU Fuses ##
By default the Whisper Node AVR is shipped with the following fuses configured:

|Fuse     |Value |
|---------|------|
|High     |0xDA  |
|Low      |0xEF  |
|Extended |0x05  |

This sets the MCU to:

* use the external Crystal and clock divider = 1 (resulting in 16MHz)
* reserve 1024 pages for the bootloader (2048 bytes)
* set the BOD Level 1 (2.7V)

## Bootloader ##
The Talk² Whisper Nodes is pre-programmed with the Talk² Boot, a modified version of the Optiboot, capable of programming the MCU from an external SPI Flash memory. For all features and documentation, please refer to the Repo https://bitbucket.org/talk2/talk2boot.

**Default Sketch Upload Speed:** 115200kbp/s

## Factory Firmware ##
Apart from the bootloader, the Talk² Whisper Node is also programmed with a factory firmware (or sketch). This firmware is used to test each board during the final phase of the manufacturing process but also to provides some additional functionalities, like the support of "Over-the-Air" updates from factory.

The same firmware is present on the MCU Flash as well in a **write-protected** area of the external SPI Flash. More specifically from the memory address 0x0007000 to 0x0007FFFF. This memory address is the same one used by the Talk² Bootloader in case a "Factory Reset Flow" is triggered.

It's 100% safe to replace the Factory Firmware with your own code, including the firmware image saved on the SPI Flash. This firmware is just installed by default for testing propose and to enable Remote Programming out-of-the-box.

**Note that if you wish to Erase the whole SPI Flash memory, you need first to disable the write-protection.*

### Factory Reset Flow ###
To trigger the "Factory Reset Flow" on the Talk² Whisper Node board, you must hold the BT1 (Button 1) and power the board. Wait until the LD1 (Blue Led) starts to blink quickly and release the button while the LD1 still blinking. The LD2 (Yellow Led) will blink quickly a few times indicating the MCU is being programmed, followed by a reset.

For all bootloader features and documentation, please refer to the Repo https://bitbucket.org/talk2/talk2boot.

### Factory Firmware Configuration ###
One way to access the firmware configuration is to simply connect it to a serial port at the speed of 115200kbp/s:

![WhisperNode_FactoryFirmware_01.png](https://bitbucket.org/repo/drxzG7/images/1675741079-WhisperNode_FactoryFirmware_01.png)

Another way to find the configuration is be access the "Factory Firmware" example code, part of the "T2WhisperNode" library.

#### Configuration Table ####

| Parameter    | 433MHz Standard | 433MHz High-Power | 868MHz Standard | 868MHz High-Power | 915MHz Standard | 915MHz High-Power |
|--------------|-----------------|-------------------|-----------------|-------------------|-----------------|-------------------|
| Frequency    | 434MHz          | 434MHz            | 868MHz          | 868MHz            | 916MHz          | 916MHz            |
| TX Power     | 13dbM           | 14dbM             | 13dbM           | 14dbM             | 13dbM           | 14dbM             |

### Remote Programming ###
TBD: How to use the Factory Firmware to remote program the Whisper Node.